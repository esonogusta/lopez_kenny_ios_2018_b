//
//  NumerosViewController.swift
//  examenLopez1B
//
//  Created by KENNY LOPEZ on 28/11/18.
//  Copyright © 2018 kl. All rights reserved.
//

import UIKit

class NumerosViewController: UIViewController {

    @IBOutlet weak var numeroTextField: UITextField!
    @IBOutlet weak var resultadoLabel: UILabel!
    let funcion = Pares()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func obtenerNumeroButtonPressed(_ sender: Any) {
        let numero = numeroTextField.text!
        let entero = Int(numero)
        
        funcion.obtenerNumeros(entero ?? 0)
        
        let stringNumero = String (funcion.serieNumeros)
        
        resultadoLabel.text = stringNumero
    }
    
    @IBAction func exitButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
