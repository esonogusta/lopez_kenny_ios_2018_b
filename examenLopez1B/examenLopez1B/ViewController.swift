//
//  ViewController.swift
//  examenLopez1B
//
//  Created by KENNY LOPEZ on 28/11/18.
//  Copyright © 2018 kl. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func loginButtonPressed(_ sender: Any) {
        let username = usernameTextField.text!
        let password = passwordTextField.text!
        
        Auth.auth().signIn(withEmail: username, password: password){
            (data, error) in
            if let error = error {
                print(error)
                return
            }
            self.performSegue(withIdentifier: "loguinSegue", sender: self)
        }
    }
    
}

